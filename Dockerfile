FROM ubuntu:18.04

ENV MIBDIR=/usr/local/share/snmp/mibs

RUN apt update
RUN apt install -y \
    snmpd \
    net-tools \
    iputils-ping \
    wget \
    gcc \
    libperl-dev \
    libtool \
    make \
    cpanminus \
    vim \
    snmp-mibs-downloader

RUN wget http://sourceforge.net/projects/net-snmp/files/net-snmp/5.8/net-snmp-5.8.tar.gz
RUN tar -xvzf net-snmp-5.8.tar.gz

WORKDIR /net-snmp-5.8
RUN ./configure \
    --with-perl-modules \
    --with-default-snmp-version="2"
RUN make
RUN make install
RUN cpan -l
RUN cpanm SNMP::Util --force

RUN download-mibs

RUN ln -s /usr/local/lib/libnetsnmp.so.35 /usr/lib/libnetsnmp.so.35
RUN ln -s /usr/local/lib/libnetsnmpagent.so.35 /usr/lib/libnetsnmpagent.so.35
RUN ln -s /usr/local/lib/libnetsnmpmibs.so.35 /usr/lib/libnetsnmpmibs.so.35
RUN sed -i "s#\(^.*'MIBDIRS'.*'\)\/.*\('.*$\)#\1$MIBDIR\2#" \
    $(find / -wholename /usr/*/Util_env.pm)

WORKDIR /
COPY ./script.sh ./root/

EXPOSE 161

VOLUME /root

CMD service snmpd start && /bin/bash