#!/usr/bin/perl
# Author: Lvnar <lvnar.dev@gmail.com>
# Name: César Pérez Canseco
# Date: May 28, 2019

use SNMP::Util;

$dev = $ENV{'DEVICE_IP'};
print "Devices: $dev\n\n";
@IP_array = split(/,/, $ENV{'DEVICE_IP'});

foreach $IP (@IP_array){
   $snmp->{$IP} = new SNMP::Util(-device => $IP,
                     -community => $public,
                     -timeout => 5,
                     -retry => 0,
                     -poll => 'off',
                     -delimiter => ' ',
                     );
}

#Now get the uptime for each switch
foreach $IP (@IP_array){
    $time = localtime(time);
    $uptime = $snmp->{$IP}->get('v','sysUpTime.0');
    $harddisk = $snmp->{$IP}->get('v','.1.3.6.1.4.1.2021.9.1.8.1');
    $memory = $snmp->{$IP}->get('v','.1.3.6.1.4.1.2021.4.6.0');
    $cpu = $snmp->{$IP}->get('v','.1.3.6.1.4.1.2021.11.9.0');
    $ifInOctets = $snmp->{$IP}->get('v','.1.3.6.1.2.1.2.2.1.10');
    $ifInOctets1 = $snmp->{$IP}->get('v','.1.3.6.1.2.1.2.2.1.10.0');
    $ifOutOctets = $snmp->{$IP}->get('v','.1.3.6.1.2.1.2.2.1.16');
    $ifSpeed = $snmp->{$IP}->get('v','.1.3.6.1.2.1.2.2.1.5');

    $res = "Uptime: $uptime\n";
    open(my $txt, '>', 'log.txt');
        print $txt "\# $IP \t$time\n";
        print $txt "Hard disk occupied: $harddisk\n";
        print $txt "Memory usage: $memory\n";
        print $txt "CPU: $cpu\n";
        print $txt "Bandwidth: $ifInOctets $ifInOctets1 $ifOutOctets $ifSpeed $\n";
        print $txt "\n\n";
    close $txt;
}
