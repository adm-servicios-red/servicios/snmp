

# Usage
    $ docker build -t snmp .
    $ docker run -it -d -v "/home/lvnar/Projects/snmp/snmpd.conf:/etc/snmp/snmpd.conf" \
                -p 161:161 \
                --name snmp \
				snmp:latest bash

# Test
    $ docker cp test.pl snmp:/root/test.pl
    $ docker exec -it snmp bash
        # cd /root
        # perl test.pl